# Socket Comms API

This document specifies the API used in communication between the main Hub-Server and the
Client-Server instance over websockets.

## Signals
The main server (or hub) is what receives HTTP requests from the client-side. 
This should not be confused with the client-server instance.
Client-side is used here to mean whichever medium through which a user wants to access his/her passwords. 
Be it a mobile app or webpage. The Hub receives those HTTP Requests and sends signals to the client's instance of
the password server. So the **Hub**, in this context, refers to the server that receives client requests, from the
**Client-Side**, and sends **signals** to the **Client-Server**, requesting data (passwords).

### The Signal Type
The `Signal` type is a record type used to encode all messages transferred between the 
Hub and Client-Server over a websocket. Without a way to send messages over the socket protocol
while still preserving the types of said message's content, messages must be parsed into
the signal on being received, by both servers.<br>
Signals are sent back and forth over the socket as JSON encoded strings.

### Signal Message Format
Signals sent from the Hub and Client-Server servers are distinct. Since the Hub server mainly requests
information, it's signals have a more complex structure. It would not be incorrect to see Hub
signals and Client signals as requests and responses respectively.<br>

#### Hub Messages
This is what typical hub signal json object looks like (so far).<br>
```json
{
	"sig": string, ❶
	"auth": object, ❷
	"body": object, ❸
	"type": string ❹
}
``` 
❶ This acts almost the exact same way as a HTTP Method. It defines the action the sender of the message wishes to carry out. See [Signal Codes](#signal-codes).

❷ This object will carry information needed to authenticate the sender of the message. See [Security](#authentication).
 
❸ Acts in the same way a request body does in a HTTP Request. In this context, it would most commonly be used for sending a new password a user might want to store.
 
❹ This option is there mostly for the convenience it provides if different data must be accessed, increasing flexibility API. It's also adds a certain amount of brevity when the type of object being acted upon must be explicitly stated.


> **NOTE**<br>
> _More properties will be added in the future, they are not all listed as this documentation and the project as a whole is still a work in progress._

Signals sent back from the client's server instance are significantly less complex. With there being only
two properties in the JSON object:
```json
{
	"response-code": string, ❶
	"response-content": string ❷
}
```

❶ This may be an error signal, e.g `MALFORMED REQUEST`.

❷ This property is optional and will not be present when responding to `STORE` requests.<br>

### Signal Codes
In the API, signal codes work much like HTTP Methods. Each signal has a corresponding set of error signals that 
can be sent back depending on the nature of the error. Error signals are purposefully verbose, for brevity and
self-documentation. All request signals will return a `MALFORMED REQUEST SIGNAL` on failure to parse.

#### FETCH
_HasBody_: **False**<br>
_Error Signals_: { **UNFOUND_RESOURCE** }<br>
The `FETCH` signal code is not dissimilar to a HTTP `GET` request. Since this is a password management service, the information needed to fetch whatever the signal is requesting is contained within the `auth` and `type` fields of the signal.

#### STORE
_HasBody_: **True**<br>
_Error Signal_: { **MALFORMED_REQUEST_BODY** }<br>
Just like a HTTP `PUT` request. The body of this signal must be another JSON encoded string, whose data will be used to construct an object of the type specified in the `type` property.

#### REMOVE
_HasBody_: **True**<br>
_Error Signal_: { **UNKNOWN_IDENTIFIER** }<br>
This signal reqeusts the deletion of an object (of type `type`) from the store. The body of the signal must be a JSON object containing only the identifier needed to find the object for deletion.

<br>
Keeping the API as simple as possible is a priority.
