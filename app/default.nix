{ mkDerivation, base, bytestring, containers, hpack, http-types
, mtl, stdenv, text, transformers, wai, wai-websockets, warp
, websockets
}:
mkDerivation {
  pname = "pass-man";
  version = "0.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
    base bytestring containers http-types mtl text transformers wai
    wai-websockets warp websockets
  ];
  preConfigure = "hpack";
  description = "Decentralized password manager";
  license = stdenv.lib.licenses.mit;
}
