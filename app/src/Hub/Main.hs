{-# LANGUAGE OverloadedStrings #-}

module Main where

import Network.Wai.Handler.WebSockets
import Network.Wai.Handler.Warp
import Network.Wai
import Network.HTTP.Types

-- The app
import Hub.Sock (routerApp, connOpts) -- For brevity

-- How the client retreives their passwords
httpApp :: Application
httpApp req res = do putStrLn $ "Using backup http application"
                     res $ responseLBS status200 [] "This API supports websockets mainly. Stahhp using http requests..." 

-- Routing server 
socketApp :: Application
socketApp = websocketsOr connOpts routerApp httpApp

main :: IO ()
main = do
  putStrLn "Running routing server..."
  run 80 socketApp
