module Hub.Sock (routerApp
                ,connOpts) where

-- Abstractions
import Control.Applicative (Alternative(..), liftA2)

-- Data types
import qualified Data.Text.Lazy as TL 

-- Transformers
import Control.Monad.Except
import Control.Monad.IO.Class (MonadIO, liftIO)

-- Websockets
import Network.WebSockets

-- Message Handler Type
import Common.MsgHandler

-- Connection options
connOpts :: ConnectionOptions
connOpts = defaultConnectionOptions

-- Application
routerApp :: ServerApp
routerApp pend = do conn <- acceptRequest pend
                    msg <- receive conn
                    handle <- msgHandler msg
                    case handle of
                      Left _ -> putStrLn "Request could not be handled"
                      Right x -> putStrLn "Request handled"

msgHandler :: Message -> IO (Either TL.Text ())
msgHandler = runMsgHandler . liftA2 (|><|) ctrlMsgHandler dataMsgHandler          

dataMsgHandler :: Message -> MsgHandler ()
dataMsgHandler msg = undefined

ctrlMsgHandler :: Message -> MsgHandler ()
ctrlMsgHandler msg = undefined
