{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Common.MsgHandler where

-- | Module for the generic MsgHandler type, used by both
--   client and routing servers

-- Data Types
import qualified Data.Text.Lazy as TL

-- Transformers 'n' Monads
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Except

newtype MsgHandler a =
  MsgHandler { unHandler :: ExceptT TL.Text IO a }
  deriving (Functor, Applicative, Monad, MonadIO, MonadError TL.Text)  

runMsgHandler :: MsgHandler a -> IO (Either TL.Text a)
runMsgHandler h = runExceptT (unHandler h)

-- Not sure if the bang patterns are actually necessary but eh
(|><|) :: MsgHandler a -> MsgHandler a -> MsgHandler a
(|><|) a b = do !evalA <- liftIO . runMsgHandler $ a
                case evalA of
                  Right x -> return x
                  Left e -> do !evalB <- liftIO . runMsgHandler $ b
                               case evalB of
                                 Right x' -> return x'
                                 Left _ -> throwError e -- Reports first error encountered if both operations fail
